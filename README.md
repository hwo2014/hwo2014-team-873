## Hello World Open (HWO) 2014 Byte Me Team Bot

This is the Bot for the [Byte Me Team](https://helloworldopen.com/team/873) in
[HWO 2014](https://helloworldopen.com/). It is implemented in Python.

### Useful Links

- [Team Profile (Pitstop)](https://helloworldopen.com/pitstop)
- [CI Test Runs](https://helloworldopen.com/pitstop/ci-runs)
- [Test Races Visualizer](https://helloworldopen.com/pitstop/testraces)
- [Competition Rules](https://helloworldopen.com/rules)
- [Bot Protocol](https://helloworldopen.com/techspec)

### File Structure And Scripts

In this directory, there are scripts used to build and run our bot.
On the C.I system and in the competition, the bot will be run on the HWO servers
using these scripts.

- `./build` - build the bot, i.e. prepare for running (creates virtualenv).
- `./run <host> <port> [<botname>]` - run the bot synchronously.
- `./test-run` - invoke `./run` with default parameters to test bot in test server.

Common configuration is done in the `config` file.

Don't modify the scripts on the main level. These scripts ensure that all bots
respond to a common control interface.

### Logging

The C.I system will capture the standard output of the bot so we can log relevant events
by just writing to stdout. Please don't log all incoming and outgoing messages though, as
that would generate too much data. They'll probably limit the maximum number of lines captured.

### External dependencies

To ensure fast build/run times, the bots are built offline on the HWO servers. This means
that the build process cannot download external dependencies from the Internet. For each
bot template we've included the necessary dependencies (sockets, JSON) either on the
HWO server platform or into this codebase. If you need anything else, you'll need to find a way to
include the extra dependencies into your codebase.

The C.I system will build and run your bot regularly to ensure that it is compatible with
the build process on the HWO servers.
