#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import json
import socket
import logging

from log import (init_logging, LOG)
from bots import NoobBot


DEBUG = False


def main(args):
    if len(args) != 4:
        print('Usage: ./%s host port botname botkey' % __name__)
    else:
        host, port, name, key = args

        logging_level = logging.DEBUG if DEBUG else logging.INFO
        init_logging('test-run', level=logging_level)
        
        LOG.info('Connecting with parameters: host=%s, port=%s, bot name=%s, key=%s', *args)
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))

        LOG.info('Connected. Running bot')
        bot = NoobBot(s, name, key)
        bot.run()

        LOG.info('Terminating.')


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        LOG.info('Interrupted by user')
    except Exception:
        LOG.exception('Unexpected exception:')
