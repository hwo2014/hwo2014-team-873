# -*- coding: utf-8 -*-

import os
import sys
import logging
import datetime


LOG_DIR = os.path.join(os.pardir, 'logs')
LOG = logging.getLogger()


def init_logging(logfile_prefix, level=logging.INFO):
    """
    Configure logging to file and console.
    """
    formatter = logging.Formatter('%(asctime)s : %(levelname)-8s : %(message)s')

    timestamp = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S_%f')
    filename = '%s_%s.log' % (logfile_prefix, timestamp)
    if not os.path.isdir(LOG_DIR):
        os.makedirs(LOG_DIR)
    logfile = os.path.join(LOG_DIR, filename)
    file_handler = logging.FileHandler(logfile)
    file_handler.setFormatter(formatter)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(formatter)

    root_logger = logging.getLogger()
    root_logger.setLevel(level)
    root_logger.addHandler(file_handler)
    root_logger.addHandler(console_handler)
